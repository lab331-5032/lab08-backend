package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Student {
    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;
}